from typing import List

import requests
from maltego_trx.entities import PhoneNumber
from maltego_trx.maltego import UIM_FATAL
from maltego_trx.transform import DiscoverableTransform


class EmailToUkrNetPhone(DiscoverableTransform):
    """
    Receive email from the client, and resolve to phone number.
    """
    @classmethod
    def create_entities(cls, request, response):
        email = request.Value

        try:
            phone_number_list = cls.get_ukr_net_phone_number_list_by_email(email)
            for phone_number in phone_number_list:
                response.addEntity(PhoneNumber, phone_number)
        except Exception as error:
            response.addUIMessage("Error: {}".format(error), UIM_FATAL)

    @staticmethod
    def get_ukr_net_phone_number_list_by_email(email: str) -> List[str]:
        response = requests.post('https://accounts.ukr.net/api/v1/cai/browser/get')
        if response.status_code == 200:
            request_id = response.json().get('id')
            if request_id:
                data = {
                    "user": email,
                    "client_application_id": request_id
                }
                response = requests.post('https://accounts.ukr.net/api/v2/recovery/init', json=data)
                if response.status_code == 200:
                    headers = {
                        'X-Rec-Sid': response.json().get('id'),
                    }
                    response = requests.post('https://accounts.ukr.net/api/v2/recovery/next',
                                             headers=headers)

                    numbers = response.json().get('contacts') or []
                    return [number.get('value') for number in numbers]
                raise Exception('Email not found')
        raise Exception('Rate limit')


if __name__ == '__main__':
    EmailToUkrNetPhone.get_ukr_net_phone_number_list_by_email('dksu_ovid@ukr.net')
